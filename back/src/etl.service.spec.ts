import { HttpService } from '@nestjs/common';
import { AppService } from './app.service';
import { EtlService } from './etl.service';

describe('ETL SERVICE', () => {
  let etlService: EtlService;
  let appService: AppService;
  let httpService: HttpService;

  beforeEach(() => {
    httpService = new HttpService();
    appService = new AppService();
    etlService = new EtlService(httpService, appService);
  });

  describe('getBaseUrl without data in db', () => {
    it('Should return url with query timestamp ', () => {
      const time = new Date().getTime();
      const templateUrl = `https://hn.algolia.com/api/v1/search_by_date?query=nodejs&numericFilters=created_at_i>${time}`;
      expect(etlService.getBaseUrl(time)).toBe(templateUrl);
    });
  });
});
