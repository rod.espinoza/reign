import { Controller, Get, Put, Query } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/hits')
  getLastHits() {
    return this.appService.getLastHits();
  }

  @Put('/hits/disable')
  disableHits(@Query('id') id) {
    return this.appService.disableById(id);
  }
}
