import { Injectable } from '@nestjs/common';

@Injectable()
export class Dummyresults {
  public dummy_data = [
    {
      created_at: '2021-05-22T22:32:42.000Z',
      title: null,
      url: null,
      author: 'bluecerulean',
      points: null,
      story_text: null,
      comment_text:
        'He most likely means that reasoners and databases that provide reasoning abilities do not scale. This makes sense, specially for OWL ontologies. For most OWL reasoners, if you feed them with the ontology and with a large set of instance data (class instances connected by edges that are labeled with properties defined in said ontology), it will likely take way more time than you would like to produce results (if it produces something).<p>The reason for that is twofold:<p>1. Many of tools created for reasoning are research-first tools. Some papers were published about the tool and it really was a petter and more scalable tool than anything before it. But every PhD student graduates and needs to find a job or move to the next hyped research area\n2. Tools are designed under the assumption that the whole ontology, all the instance data and all results fit in main memory (RAM). This assumption is de-facto necessary for more powerful entailment regimes of OWL.<p>Reason 2 as a secondary sub-reason that OWL ontologies use URIs (actually IRIs), which are really inneficient identifiers compared to 32&#x2F;64-bit integers. HDT is a format that fixes this inneficiency for RDF (and thus is applicable to ontologies) but since it came about nearly all reasoners where already abandoned as per reason #1 above.<p>Newer reasoners that actually scale quite a bit are RDFox [1] and VLog [2]. They use compact representations and try to be nice with the CPU cache and pipeline. However, they are limited to a single shared memory (even if NUMA).<p>There is a lot of mostly academic distributed reasoners designed to scale horizontally instead of vertically. These systems technically scale, but vertically scaling the centralized aforementioned systems will be more efficient. The intrinsic problem with distributing is that (i) it is hard to partition the input aiming at a fair distribution of work and (ii) inferred facts derived at one node often are evidence that multiple other nodes need to known.<p>loose from modern single-node  However, the problem of computing all inferred edges from a knowledge graph involves a great deal of communication, since one inference found by one node is evidence required by another processing node.<p>[1]: <a href="https:&#x2F;&#x2F;www.oxfordsemantic.tech&#x2F;product" rel="nofollow">https:&#x2F;&#x2F;www.oxfordsemantic.tech&#x2F;product</a>\n[2]: <a href="https:&#x2F;&#x2F;github.com&#x2F;karmaresearch&#x2F;vlog&#x2F;" rel="nofollow">https:&#x2F;&#x2F;github.com&#x2F;karmaresearch&#x2F;vlog&#x2F;</a>',
      num_comments: null,
      story_id: 27245696,
      story_title: 'An Introduction to Knowledge Graphs',
      story_url:
        'http://ai.stanford.edu/blog/introduction-to-knowledge-graphs/',
      parent_id: 27250551,
      created_at_i: 1621722762,
      _tags: ['comment', 'author_bluecerulean', 'story_27245696'],
      objectID: '27250845',
      _highlightResult: {
        author: {
          value: 'bluecerulean',
          matchLevel: 'none',
          matchedWords: [],
        },
        comment_text: {
          value:
            'He most likely means that reasoners and databases that provide reasoning abilities do not scale. This makes sense, specially for OWL ontologies. For most OWL reasoners, if you feed them with the ontology and with a large set of instance data (class instances connected by edges that are labeled with properties defined in said ontology), it will likely take way more time than you would like to produce results (if it produces something).<p>The reason for that is twofold:<p>1. Many of tools created for reasoning are research-first tools. Some papers were published about the tool and it really was a petter and more scalable tool than anything before it. But every PhD student graduates and needs to find a job or move to the next hyped research area\n2. Tools are designed under the assumption that the whole ontology, all the instance data and all results fit in main memory (RAM). This assumption is de-facto necessary for more powerful entailment regimes of OWL.<p>Reason 2 as a secondary sub-reason that OWL ontologies use URIs (actually IRIs), which are really inneficient identifiers compared to 32/64-bit integers. HDT is a format that fixes this inneficiency for RDF (and thus is applicable to ontologies) but since it came about nearly all reasoners where already abandoned as per reason #1 above.<p>Newer reasoners that actually scale quite a bit are RDFox [1] and VLog [2]. They use compact representations and try to be nice with the CPU cache and pipeline. However, they are limited to a single shared memory (even if NUMA).<p>There is a lot of mostly academic distributed reasoners designed to scale horizontally instead of vertically. These systems technically scale, but vertically scaling the centralized aforementioned systems will be more efficient. The intrinsic problem with distributing is that (i) it is hard to partition the input aiming at a fair distribution of work and (ii) inferred facts derived at one node often are evidence that multiple other <em>nodes</em> need to known.<p>loose from modern single-node  However, the problem of computing all inferred edges from a knowledge graph involves a great deal of communication, since one inference found by one node is evidence required by another processing node.<p>[1]: <a href="https://www.oxfordsemantic.tech/product" rel="nofollow">https://www.oxfordsemantic.tech/product</a>\n[2]: <a href="https://github.com/karmaresearch/vlog/" rel="nofollow">https://github.com/karmaresearch/vlog/</a>',
          matchLevel: 'full',
          fullyHighlighted: false,
          matchedWords: ['nodejs'],
        },
        story_title: {
          value: 'An Introduction to Knowledge Graphs',
          matchLevel: 'none',
          matchedWords: [],
        },
        story_url: {
          value:
            'http://ai.stanford.edu/blog/introduction-to-knowledge-graphs/',
          matchLevel: 'none',
          matchedWords: [],
        },
      },
    },
    {
      created_at: '2021-05-22T21:15:59.000Z',
      title: null,
      url: null,
      author: 'zozbot234',
      points: null,
      story_text: null,
      comment_text:
        'The definition seems faulty to me, since the pair (E: subset(N × N), f: E → L) does not admit of multiple edges with different labels, connecting the same ordered pair of nodes.  Of course this is most often allowed in practical KG&#x27;s.',
      num_comments: null,
      story_id: 27245696,
      story_title: 'An Introduction to Knowledge Graphs',
      story_url:
        'http://ai.stanford.edu/blog/introduction-to-knowledge-graphs/',
      parent_id: 27245696,
      created_at_i: 1621718159,
      _tags: ['comment', 'author_zozbot234', 'story_27245696'],
      objectID: '27250337',
      _highlightResult: {
        author: {
          value: 'zozbot234',
          matchLevel: 'none',
          matchedWords: [],
        },
        comment_text: {
          value:
            "The definition seems faulty to me, since the pair (E: subset(N × N), f: E → L) does not admit of multiple edges with different labels, connecting the same ordered pair of <em>nodes</em>.  Of course this is most often allowed in practical KG's.",
          matchLevel: 'full',
          fullyHighlighted: false,
          matchedWords: ['nodejs'],
        },
        story_title: {
          value: 'An Introduction to Knowledge Graphs',
          matchLevel: 'none',
          matchedWords: [],
        },
        story_url: {
          value:
            'http://ai.stanford.edu/blog/introduction-to-knowledge-graphs/',
          matchLevel: 'none',
          matchedWords: [],
        },
      },
    },
    {
      created_at: '2021-05-22T20:53:42.000Z',
      title: null,
      url: null,
      author: 'Zenst',
      points: null,
      story_text: null,
      comment_text:
        '&quot;Ptychography works by scanning overlapping scattering patterns from a material sample and looking for changes in the overlapping region&quot;<p>Interesting, we use multi-patterning to produce silicon already to get the light down to scale for process nodes - this is kinda the reverse and do wonder if some of this research may have applications in silicon production.',
      num_comments: null,
      story_id: 27247605,
      story_title: 'Cornell researchers see atoms at record resolution',
      story_url:
        'https://news.cornell.edu/stories/2021/05/cornell-researchers-see-atoms-record-resolution',
      parent_id: 27247605,
      created_at_i: 1621716822,
      _tags: ['comment', 'author_Zenst', 'story_27247605'],
      objectID: '27250174',
      _highlightResult: {
        author: {
          value: 'Zenst',
          matchLevel: 'none',
          matchedWords: [],
        },
        comment_text: {
          value:
            '&quot;Ptychography works by scanning overlapping scattering patterns from a material sample and looking for changes in the overlapping region&quot;<p>Interesting, we use multi-patterning to produce silicon already to get the light down to scale for process <em>nodes</em> - this is kinda the reverse and do wonder if some of this research may have applications in silicon production.',
          matchLevel: 'full',
          fullyHighlighted: false,
          matchedWords: ['nodejs'],
        },
        story_title: {
          value: 'Cornell researchers see atoms at record resolution',
          matchLevel: 'none',
          matchedWords: [],
        },
        story_url: {
          value:
            'https://news.cornell.edu/stories/2021/05/cornell-researchers-see-atoms-record-resolution',
          matchLevel: 'none',
          matchedWords: [],
        },
      },
    },
    {
      created_at: '2021-05-22T20:27:21.000Z',
      title: null,
      url: null,
      author: 'JoelJacobson',
      points: null,
      story_text: null,
      comment_text:
        'SQL might be a good fit to model Knowledge Graphs, since FOREIGN KEYs can be named, using the CONSTRAINT constraint_name FOREIGN KEY … syntax. We thus have support to label edges.<p>Nodes = Tables<p>Edges = Foreign keys<p>Edge labels = Foreign key constraint names',
      num_comments: null,
      story_id: 27245696,
      story_title: 'An Introduction to Knowledge Graphs',
      story_url:
        'http://ai.stanford.edu/blog/introduction-to-knowledge-graphs/',
      parent_id: 27245696,
      created_at_i: 1621715241,
      _tags: ['comment', 'author_JoelJacobson', 'story_27245696'],
      objectID: '27249983',
      _highlightResult: {
        author: {
          value: 'JoelJacobson',
          matchLevel: 'none',
          matchedWords: [],
        },
        comment_text: {
          value:
            'SQL might be a good fit to model Knowledge Graphs, since FOREIGN KEYs can be named, using the CONSTRAINT constraint_name FOREIGN KEY … syntax. We thus have support to label edges.<p><em>Nodes</em> = Tables<p>Edges = Foreign keys<p>Edge labels = Foreign key constraint names',
          matchLevel: 'full',
          fullyHighlighted: false,
          matchedWords: ['nodejs'],
        },
        story_title: {
          value: 'An Introduction to Knowledge Graphs',
          matchLevel: 'none',
          matchedWords: [],
        },
        story_url: {
          value:
            'http://ai.stanford.edu/blog/introduction-to-knowledge-graphs/',
          matchLevel: 'none',
          matchedWords: [],
        },
      },
    },
    {
      created_at: '2021-05-22T17:39:47.000Z',
      title: null,
      url: null,
      author: 'aszen',
      points: null,
      story_text: null,
      comment_text:
        'i would say golang and nodejs are more popular for api&#x27;s, python is also popular with flask and fast api.\nlastly my experience is graphql is getting very popular for building api&#x27;s that power the front end so learning that would also be advantageous.',
      num_comments: null,
      story_id: 27246347,
      story_title: 'Ask HN: What should I learn next; Rust or Elixir?',
      story_url: null,
      parent_id: 27246347,
      created_at_i: 1621705187,
      _tags: ['comment', 'author_aszen', 'story_27246347'],
      objectID: '27248672',
      _highlightResult: {
        author: {
          value: 'aszen',
          matchLevel: 'none',
          matchedWords: [],
        },
        comment_text: {
          value:
            "i would say golang and <em>nodejs</em> are more popular for api's, python is also popular with flask and fast api.\nlastly my experience is graphql is getting very popular for building api's that power the front end so learning that would also be advantageous.",
          matchLevel: 'full',
          fullyHighlighted: false,
          matchedWords: ['nodejs'],
        },
        story_title: {
          value: 'Ask HN: What should I learn next; Rust or Elixir?',
          matchLevel: 'none',
          matchedWords: [],
        },
      },
    },
  ];
}
