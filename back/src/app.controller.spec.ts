import { AppService } from './app.service';
import { Dummyresults } from './dummy/dummy_data';

describe('AppController', () => {
  let appService: AppService;
  let dummy: Dummyresults;
  beforeEach(async () => {
    appService = new AppService();
    dummy = new Dummyresults();
    await appService.insertMany(dummy.dummy_data);
  });

  describe('getLast Clean hits', () => {
    it('should return "5 docs"', async () => {
      const result = await appService.getLastHits();
      expect(result.length).toBe(5);
    });
  });
});
