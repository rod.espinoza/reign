import { Injectable } from '@nestjs/common';
import { MongoClient } from 'mongodb';
// TO DO REFACTOR ENVIROMENT VARIABLES BASED IN LINT OR DOCKERIZATION
const url = process.env.MONGO
  ? process.env.MONGO
  : 'mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false';
const client = new MongoClient(url);

@Injectable()
export class AppService {
  async getLastHits(): Promise<any[]> {
    await client.connect();
    return new Promise((resolve) => {
      client
        .db('reign')
        .collection('hits')
        .find({ deactivate: { $exists: false } })
        .sort({ created_at_i: -1 })
        .limit(10)
        .toArray((error, result) => {
          if (error) throw error;
          resolve(result);
        });
    });
  }

  async getHitsInArray(values: any[]): Promise<any[]> {
    await client.connect();
    const objectsIds = values.map((v) => v.objectID);
    return new Promise((resolve) => {
      client
        .db('reign')
        .collection('hits')
        .find({ objectID: { $in: objectsIds } })
        .toArray((error, result) => {
          if (error) throw error;
          resolve(result);
        });
    });
  }

  async disableById(id: string) {
    await client.connect();
    const query = { objectID: id };
    const newvalues = { $set: { deactivate: true } };
    return new Promise((resolve) => {
      client
        .db('reign')
        .collection('hits')
        .updateOne(query, newvalues, (error, res) => {
          if (error) throw error;
          resolve(res);
        });
    });
  }

  async insertMany(list) {
    await client.connect();
    return new Promise((resolve) => {
      client
        .db('reign')
        .collection('hits')
        .insertMany(list, (error, res) => {
          if (error) throw error;
          resolve(res);
        });
    });
  }
  async deleteAll() {
    await client.connect();
    return new Promise((resolve) => {
      client
        .db('reign')
        .collection('hits')
        .drop((err, res) => {
          if (err) throw err;
          resolve(res);
        });
    });
  }

  async getMaxTimeStamp() {
    await client.connect();
    return new Promise((resolve) => {
      client
        .db('reign')
        .collection('hits')
        .find()
        .sort({ created_at_i: -1 })
        .limit(1)
        .toArray((error, res) => {
          if (error) throw error;
          if (res.length > 0) {
            resolve(res[0]['created_at_i']);
          } else {
            resolve(null);
          }
        });
    });
  }
}
