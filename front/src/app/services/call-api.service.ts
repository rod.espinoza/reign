import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { environment } from "src/environments/environment";
import { HttpClient  } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class CallApiService {
    BASE_URI = environment.project_api_uri;
    
    constructor(private http:HttpClient){}
    
    getLastHits():Observable<any>{
        return this.httpGet(`${this.BASE_URI}hits`);
    }

    disableByUrl(id:string):Observable<any>{
        return this.http.put(`${this.BASE_URI}hits/disable?id=${id}`, {})
    }


    httpGet(complete_uri:string): Observable<any> {
      return this.http.get<any>(complete_uri).pipe(
        retry(1), catchError(this.errorHandl)
      )
    }

    errorHandl(error: any) {
        let errorMessage = '';
        if(error.error instanceof ErrorEvent) {
          // Get client-side error
          errorMessage = error.error.message;
        } else {
          // Get server-side error
          errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        return throwError(errorMessage);
     }
}