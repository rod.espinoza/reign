import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CallApiService } from '../services/call-api.service';

@Component({
  selector: 'app-hits',
  templateUrl: './hits.component.html',
  styleUrls: ['./hits.component.css']
})
export class HitsComponent implements OnInit {
  subscriptionTen: Subscription | undefined;
  subscriptionTurnOff: Subscription | undefined;
  hits:any[] = [];

  constructor(private callApiService: CallApiService) { }

  ngOnInit(): void {
    this.getLastHits();
  }
  getLastHits() {
    this.hits = [];
    this.subscriptionTen = this.callApiService.getLastHits().subscribe(result => {
      console.log(result)
      this.hits = result;
    })
  }

  openLink(hit:any){
    console.log("openLink")
    window.open(hit.url || hit.story_url, "_blank");
  }
  disableItem(hit:any){
    console.log(hit)
    if(!hit) return;
    this.subscriptionTurnOff = this.callApiService.disableByUrl(hit.objectID).subscribe(
      result => {
        this.getLastHits();
      })

  }
}
